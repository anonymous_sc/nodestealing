#!/usr/bin/bash

# run simulation of heuristic h0 and h111 for June 2017

printf "The synthetic simulation is running\n"
for e in {0..4}
do
    printf "The failure scenario %d is running\n" $e

    cd test_synthetic_${e}_0
    robin ./test_synthetic_${e}_0.yaml > outputfile 2> error_file &
    cd ..

    cd test_synthetic_${e}_111
    robin ./test_synthetic_${e}_111.yaml > outputfile 2> error_file &
    cd ..
done 

wait

printf "The synthetic without failures simulation is running\n"
# no node stealing, simple chechpoints using original conservative backfilling heuristic 2013March
cd test_synthetic_withoutC

robin ./test_synthetic_withoutC.yaml

cd ..