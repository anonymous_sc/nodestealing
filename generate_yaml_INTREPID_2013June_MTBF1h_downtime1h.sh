#!/usr/bin/bash

### generate yaml files of heuristic h111 for June 2013

for e in {0..4}
do
    mkdir test_INTREPID_2013June_MTBF1h_downtime1h_${e}_0
    cd test_INTREPID_2013June_MTBF1h_downtime1h_${e}_0

    # no node stealing, simple chechpoints
    robin generate "test_INTREPID_2013June_MTBF1h_downtime1h_${e}_0.yaml" \
    --output-dir "log_${e}_0" \
    --batcmd "batsim -p ../platforms/cluster40960.xml -w ../workloads/INTREPID_2013June_MTBF1h.json -e log_${e}_0/expe_${e}_0 --enable-dynamic-jobs --enable-profile-reuse --acknowledge-dynamic-jobs --forward-profiles-on-submission --events ../events/events_INTREPID_MTBF1h_downtime1h_$e.txt --disable-schedule-tracing --socket-endpoint ipc://foobar_${e}_0" \
    --schedcmd "batsched -v conservative_bf_node_stealing --variant_options={\\\"heuristic_choice\\\":0\,\\\"victim_choice\\\":1\,\\\"decision_choice\\\":1} --queue_order fcfs_prio --socket-endpoint ipc://foobar_${e}_0" 

    cd ..
    
    # all 18 node stealing heuristics
    for h in 1
    do
        for v in 1
        do
            for k in 1
            do
                mkdir test_INTREPID_2013June_MTBF1h_downtime1h_${e}_$h$v$k
                cd test_INTREPID_2013June_MTBF1h_downtime1h_${e}_$h$v$k

                robin generate "test_INTREPID_2013June_MTBF1h_downtime1h_${e}_$h$v$k.yaml" \
                --output-dir "log_${e}_$h$v$k" \
                --batcmd "batsim -p ../platforms/cluster40960.xml -w ../workloads/INTREPID_2013June_MTBF1h.json -e log_${e}_$h$v$k/expe_${e}_$h$v$k --enable-dynamic-jobs --enable-profile-reuse --acknowledge-dynamic-jobs --forward-profiles-on-submission --events ../events/events_INTREPID_MTBF1h_downtime1h_$e.txt --disable-schedule-tracing --socket-endpoint ipc://foobar_${e}_$h$v$k" \
                --schedcmd "batsched -v conservative_bf_node_stealing --variant_options={\\\"heuristic_choice\\\":$h\,\\\"victim_choice\\\":$v\,\\\"decision_choice\\\":$k} --queue_order fcfs_prio --socket-endpoint ipc://foobar_${e}_$h$v$k" 
            
                cd ..
            done
        done
    done 
done


# generate yaml files of original heuristic conservative_bf for June 2013 without checkpoints
mkdir test_INTREPID_2013June_MTBF1h_downtime1h_withoutC
cd test_INTREPID_2013June_MTBF1h_downtime1h_withoutC
 
robin generate "test_INTREPID_2013June_MTBF1h_downtime1h_withoutC.yaml" \
--output-dir "expe-out_INTREPID_2013June_MTBF1h_downtime1h_only_conservativebf" \
--batcmd "batsim -p ../platforms/cluster40960.xml -w ../workloads/INTREPID_2013June_withoutC.json -e expe-out_INTREPID_2013June_MTBF1h_downtime1h_only_conservativebf/expe_0_0 --disable-schedule-tracing" \
--schedcmd "batsched -v conservative_bf"  

cd ..