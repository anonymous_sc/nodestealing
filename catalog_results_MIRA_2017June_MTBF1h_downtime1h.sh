#!/usr/bin/bash

### catalog h0 and h111 simulation results for 2018March

mkdir test_MIRA_2017June_MTBF1h_downtime1h

for e in {0..4}
do
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0/COMPLETED_KILLED.txt ./test_MIRA_2017June_MTBF1h_downtime1h/COMPLETED_KILLED_${e}_0.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_MIRA_2017June_MTBF1h_downtime1h/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_0.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0/node_stealing_info.txt ./test_MIRA_2017June_MTBF1h_downtime1h/node_stealing_info_${e}_0.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0/log_${e}_0/expe_${e}_0_jobs.csv ./test_MIRA_2017June_MTBF1h_downtime1h/expe_${e}_0_jobs.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0/log_${e}_0/expe_${e}_0_machine_states.csv ./test_MIRA_2017June_MTBF1h_downtime1h/expe_${e}_0_machine_states.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0/log_${e}_0/expe_${e}_0_schedule.csv ./test_MIRA_2017June_MTBF1h_downtime1h/expe_${e}_0_schedule.csv


    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/COMPLETED_KILLED.txt ./test_MIRA_2017June_MTBF1h_downtime1h/COMPLETED_KILLED_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_MIRA_2017June_MTBF1h_downtime1h/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/node_stealing_info.txt ./test_MIRA_2017June_MTBF1h_downtime1h/node_stealing_info_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/node_stealing_times.txt ./test_MIRA_2017June_MTBF1h_downtime1h/node_stealing_times_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/log_${e}_111/expe_${e}_111_jobs.csv ./test_MIRA_2017June_MTBF1h_downtime1h/expe_${e}_111_jobs.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/log_${e}_111/expe_${e}_111_machine_states.csv ./test_MIRA_2017June_MTBF1h_downtime1h/expe_${e}_111_machine_states.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111/log_${e}_111/expe_${e}_111_schedule.csv ./test_MIRA_2017June_MTBF1h_downtime1h/expe_${e}_111_schedule.csv
done 

for e in {0..4}
do
    rm -r test_MIRA_2017June_MTBF1h_downtime1h_${e}_0
    rm -r test_MIRA_2017June_MTBF1h_downtime1h_${e}_111
done 