rm(list=ls())
library(ggplot2)
library(dplyr)
setwd("../")
root_dir = getwd()

for(jj in 1:2){
  print(jj)
  if(jj==1){
    setwd(paste(root_dir,"/MIRA_simulation_data/MIRA2017June/test_MIRA_2017June_MTBF1h_downtime1h_allheuristics",sep=""))
  }
  else{
    setwd(paste(root_dir,"/MIRA_simulation_data/MIRA2018March/test_MIRA_2018March_MTBF1h_downtime1h_allheuristics",sep=""))
  }
  
  event_repeat_times=5
  max_all = vector()
  mean_all = vector()
  for(nn in 0:(event_repeat_times-1)){
    print(nn)
    max_heuristic_all = vector()
    mean_heuristic_all = vector()
    filename0=paste("expe", nn, "0_jobs.csv", collapse = ", ", sep = "_")
    
    filename111=paste("expe", nn, "111_jobs.csv", collapse = ", ", sep = "_")
    filename112=paste("expe", nn, "112_jobs.csv", collapse = ", ", sep = "_")
    filename113=paste("expe", nn, "113_jobs.csv", collapse = ", ", sep = "_")
    files<-c(filename0,filename111,filename112,filename113)
    
    for (ii in 1:length(files)){
      data <- read.csv(file=files[ii], header=T,stringsAsFactors = FALSE)
      data <- data %>% filter(data$final_state != "COMPLETED_WALLTIME_REACHED")
      data = data[!grepl("checkpoint", data[,1]),] #remove checkpoint job
      
      w0_job = data %>% filter(data$workload_name == "w0")
      w0_job=w0_job[order(w0_job$submission_time),]
      begin = 0.2*max(w0_job$submission_time)
      end = 0.8*max(w0_job$submission_time)
      
      data_remove_job =  w0_job %>% filter(w0_job$submission_time < begin | w0_job$submission_time > end)
      #First, we remove data_remove_job_succeed
      data_remove_job_succeed = data_remove_job %>% filter(data_remove_job$final_state == "COMPLETED_SUCCESSFULLY") 
      
      for (i0 in 1:length(data_remove_job_succeed$job_id)){
        job_remove_temp = data_remove_job_succeed$job_id[i0]
        data = data %>% filter(data$job_id != job_remove_temp)
      }
      
      #Second, we remove data_remove_job_killed and their job copies
      data_remove_job_killed = data_remove_job %>% filter(data_remove_job$final_state == "COMPLETED_KILLED") 
      data_lastjobcopy = data %>% filter(data$workload_name=="resubmit" & data$final_state=="COMPLETED_SUCCESSFULLY")
      job_remove_killed = data_remove_job_killed$job_id
      for(i1 in 1:length(job_remove_killed)){
        job_remove = job_remove_killed[i1]
        data = data %>% filter(data$job_id != job_remove) #The original job should be removed like 29028
        for (i2 in 1:length(data_lastjobcopy$job_id)){
          if (grepl(job_remove,data_lastjobcopy$job_id[i2])){
            job_remove_split = strsplit(data_lastjobcopy$job_id[i2], "-")
            resubmit_times = as.numeric(job_remove_split[[1]][2])
            for (i3 in 1:resubmit_times){
              #remove each intermediate job until the last job copt
              job_remove_temp = paste(job_remove, i3, collapse="," ,sep = "-") 
              data = data %>% filter(data$job_id != job_remove_temp) #The job copy should also be removed like 29028-1 and 29028-2
            }
          }
        }
      }
      
      data_jobfristsucceed <- data %>% filter(data[,2] == "w0" & as.numeric(data[,7])==1)
      data_jobfristsucceed <- data_jobfristsucceed%>%dplyr::mutate(flow =  as.numeric(data_jobfristsucceed[,11]) - as.numeric(data_jobfristsucceed[,4])) 
      data_jobfristsucceed_flow = select(data_jobfristsucceed,c(1,5,18))
      data_jobnotfristsucceed <- data %>% filter(data[,2] != "w0" | as.numeric(data[,7])==0)
      
      m=length(data_jobnotfristsucceed[,1])
      job_id=vector()
      requested_number_of_resources=vector()
      flow=vector()
      for (i in 1:m){
        id = data_jobnotfristsucceed[c(i),c(1)]
        a=grepl('checkpoint',id)
        if(a!=TRUE){
          #not a checkpoint job 
          b=grepl('-',id)
          if (b!=TRUE){
            #not a resubmitted copy job
            # a normal job and not complete successfully in the first normal time
            c=1
            d=paste(id,"-",c,sep = "")
            for (j in (i+1):m){
              if(j>m){
                break
              }
              if(data_jobnotfristsucceed[c(j),c(1)]==d)
              {
                #find the resubmitted copy of the normal job
                if(data_jobnotfristsucceed[c(j),c(7)]==1) #remark: very special job COMPLETED_WALLTIME_REACHED, the completed job copy could never be found
                {
                  #the resubmitted copy succeed in the first resubmitted time 
                  real_number_id = data_jobnotfristsucceed[c(i),c(1)] #Here is i, because the id/submission time/res is according to the original index like 7
                  job_id <- append(job_id,real_number_id)
                  submission_time = data_jobnotfristsucceed[c(i),c(4)]
                  res = data_jobnotfristsucceed[c(i),c(5)]
                  requested_number_of_resources<-append(requested_number_of_resources,res)
                  
                  finish_time = data_jobnotfristsucceed[c(j),c(11)] #Here is j, because the finish time is according to the final resubmitted index like 7-3
                  flow_temp = finish_time - submission_time
                  flow <- append(flow,flow_temp)
                  break
                }
                else
                {
                  #the resubmitted copy not succeed in the first resubmitted time, job id like 7-2
                  c=c+1
                  d=paste(id,"-",c,sep = "")
                }
              }
            }
          }
        }
      }
      
      data_jobnotfristsucceed_flow <- data.frame(job_id,requested_number_of_resources,flow,stringsAsFactors = FALSE)
      new_data <- rbind(data_jobfristsucceed_flow, data_jobnotfristsucceed_flow)
      
      new_data_small <- new_data %>% filter(as.numeric(new_data[,2]) >= 1 & as.numeric(new_data[,2]) < 2^7)
      max_flow_small_heuristic = max(as.numeric(new_data_small[,3]))
      mean_flow_small_heuristic = mean(as.numeric(new_data_small[,3]))
      
      new_data16 <- new_data %>% filter(as.numeric(new_data[,2]) >= 2^7 & as.numeric(new_data[,2]) < 2^9)
      max_flow16_heuristic = max(as.numeric(new_data16[,3]))
      mean_flow16_heuristic = mean(as.numeric(new_data16[,3]))
      
      new_data32 <- new_data %>% filter(as.numeric(new_data[,2]) >= 2^9 & as.numeric(new_data[,2]) < 2^11)
      max_flow32_heuristic = max(as.numeric(new_data32[,3]))
      mean_flow32_heuristic = mean(as.numeric(new_data32[,3]))
      
      new_data64 <- new_data %>% filter(as.numeric(new_data[,2]) >= 2^11 & as.numeric(new_data[,2]) < 2^13)
      max_flow64_heuristic = max(as.numeric(new_data64[,3]))
      mean_flow64_heuristic = mean(as.numeric(new_data64[,3]))
      
      new_data128 <- new_data %>% filter(as.numeric(new_data[,2]) >= 2^13 & as.numeric(new_data[,2]) < 2^15)
      max_flow128_heuristic = max(as.numeric(new_data128[,3]))
      mean_flow128_heuristic = mean(as.numeric(new_data128[,3]))
      
      new_data256 <- new_data %>% filter(as.numeric(new_data[,2]) >= 2^15 & as.numeric(new_data[,2]) < 2^16)
      max_flow256_heuristic = max(as.numeric(new_data256[,3]))
      mean_flow256_heuristic = mean(as.numeric(new_data256[,3]))
      
      m_heuristic=length(new_data[,1])
      max_flow_all_heuristic = max(as.numeric(new_data[,3]))
      mean_flow_all_heuristic = mean(as.numeric(new_data[,3]))
      weighted_mean_flow_all_heuristic = sum(new_data$requested_number_of_resources*new_data$flow)/sum(new_data$requested_number_of_resources)
      
      
      if(ii==1){
        max_heuristic_temp_h0 = c(max_flow_small_heuristic,max_flow16_heuristic,max_flow32_heuristic,max_flow64_heuristic,max_flow128_heuristic,max_flow256_heuristic,max_flow_all_heuristic)
        mean_heuristic_temp_h0 = c(mean_flow_small_heuristic,mean_flow16_heuristic,mean_flow32_heuristic,mean_flow64_heuristic,mean_flow128_heuristic,mean_flow256_heuristic,mean_flow_all_heuristic,weighted_mean_flow_all_heuristic)
      }
      else{
        max_heuristic_temp = c(max_flow_small_heuristic,max_flow16_heuristic,max_flow32_heuristic,max_flow64_heuristic,max_flow128_heuristic,max_flow256_heuristic,max_flow_all_heuristic)
        max_heuristic_all = c(max_heuristic_all,max_heuristic_temp/max_heuristic_temp_h0)
        mean_heuristic_temp = c(mean_flow_small_heuristic,mean_flow16_heuristic,mean_flow32_heuristic,mean_flow64_heuristic,mean_flow128_heuristic,mean_flow256_heuristic,mean_flow_all_heuristic,weighted_mean_flow_all_heuristic)
        mean_heuristic_all = c(mean_heuristic_all,mean_heuristic_temp/mean_heuristic_temp_h0)
      }
    }
    max_all = c(max_all,max_heuristic_all)
    mean_all = c(mean_all,mean_heuristic_all)
  }
  
  if(jj==1){
    max_all_2017June = max_all
    mean_all_2017June = mean_all
  }
  else{
    max_all_2018March = max_all
    mean_all_2018March = mean_all
  }
}

strategy_max = c(rep(c(rep("Variant111", times=7), rep("Variant112", times=7), rep("Variant113", times=7)),times=event_repeat_times))
strategy_mean = c(rep(c(rep("Variant111", times=8), rep("Variant112", times=8), rep("Variant113", times=8)),times=event_repeat_times))

xdata_max<-factor(c("[1,2^7)", "[2^7,2^9)", "[2^9,2^11)", "[2^11,2^13)", "[2^13,2^15)","[2^15,2^16)","all"),levels=c("[1,2^7)", "[2^7,2^9)", "[2^9,2^11)", "[2^11,2^13)", "[2^13,2^15)","[2^15,2^16)","all"))
df_max_2017June <- data.frame(strategy = strategy_max,
                              xdata = xdata_max,
                              ydata = max_all_2017June,
                              type = rep(c("Maximum flow"),times=7*(length(files)-1)*5), # 7 different job size * 3 heuristics * 5 repeat times
                              data = rep(c("2017June"),times=7*(length(files)-1)*5))

xdata_mean<-factor(c("[1,2^7)", "[2^7,2^9)", "[2^9,2^11)", "[2^11,2^13)", "[2^13,2^15)","[2^15,2^16)", "all", "weighted"),levels=c("[1,2^7)", "[2^7,2^9)", "[2^9,2^11)", "[2^11,2^13)", "[2^13,2^15)","[2^15,2^16)", "all", "weighted"))
df_mean_2017June <- data.frame(strategy = strategy_mean,
                               xdata = xdata_mean,
                               ydata=mean_all_2017June,
                               type = rep(c("Mean flow"),times=8*(length(files)-1)*5),
                               data = rep(c("2017June"),times=8*(length(files)-1)*5))

df_max_2018March <- data.frame(strategy = strategy_max,
                               xdata = xdata_max,
                               ydata = max_all_2018March,
                               type = rep(c("Maximum flow"),times=7*(length(files)-1)*5),
                               data = rep(c("2018March"),times=7*(length(files)-1)*5))

df_mean_2018March <- data.frame(strategy = strategy_mean,
                                xdata = xdata_mean,
                                ydata=mean_all_2018March,
                                type = rep(c("Mean flow"),times=8*(length(files)-1)*5),
                                data = rep(c("2018March"),times=8*(length(files)-1)*5))

df = rbind(df_max_2017June, df_mean_2017June, df_max_2018March, df_mean_2018March)

df <- df %>% filter(df$ydata <= 3) #remove outliers greater than 3

p<-ggplot(data=df, aes(x=xdata,y=ydata,fill=strategy)) + geom_boxplot(outlier.size = 0.5)+
  stat_summary(fun="mean", geom="point", size=2, position=position_dodge(width=0.75), color="black",pch=4)

p+ xlab("Job Size") + theme(axis.title.x = element_text(size = 14, angle = 0),axis.title.y = element_text(size = 14, angle = 90)) +
  theme(plot.title=element_text(hjust=0.5)) + theme(legend.title = element_blank(), legend.text = element_text(size = 15), legend.position = "top") + scale_fill_discrete(name = "heuristic") + facet_grid(data~type, scales = "free") + ylab(NULL) + 
  theme(axis.text.x = element_text(size = 10, angle = 270, hjust = 0.5, vjust = 0.5)) +
  theme(axis.text.y = element_text(size = 10)) 

setwd(paste(root_dir,"/Rplot",sep=""))
ggsave("Figure17.pdf", width=5, height=5)