rm(list=ls())
library(dplyr)
library(xtable)
setwd("../")
root_dir = getwd()

MTBF = 3600
nb_computing_machines = 49152
begin = 10*24*3600
end = 30*24*3600
downtime = "downtime1h"
date_choice = c("2017June","2018March")
for(ii in 1:length(date_choice)){
  date = date_choice[ii]
  wdfile = paste(root_dir,"/MIRA_simulation_data/MIRA", date,"/test_MIRA_", date,"_MTBF1h_", downtime, "_only_conservativebf", collapse = ", ", sep = "")
  setwd(wdfile)
  
  datafile = paste("./expe_0_0_jobs.csv", collapse = ", ", sep = "")
  data <- read.csv(file = datafile, header=T,stringsAsFactors = FALSE)
  data_w0_successful <- data %>% filter(data[,2] == "w0" & data[,8] == "COMPLETED_SUCCESSFULLY")
  useful = data_w0_successful$execution_time * data_w0_successful$requested_number_of_resources
  data_w0_successful<-cbind(data_w0_successful,useful)
  data_remove_begin <- data %>% filter(data[,11] < begin) #finish_time before bound_begin
  job_list_removebegin = data_remove_begin[,1]
  for (i in 1:length(job_list_removebegin)){
    data_w0_successful = data_w0_successful[data_w0_successful$job_id!=job_list_removebegin[i], ]
  }
  # remove data_remove_end from data_completed_killed, data_real_completed_successfully_jobcopy and data_w0_successful
  data_remove_end <- data %>% filter( data[,9]>end) #start_time after bound_begin
  job_list_removeend = data_remove_end[,1]
  for (i in 1:length(job_list_removeend)){
    data_w0_successful = data_w0_successful[data_w0_successful$job_id!=job_list_removeend[i], ]
  }
  
  # deal with jobs cross the begin bound
  data_cross_begin <- data %>% filter(data[,9] < begin & data[,11] > begin) #starting_time before bound_begin and finish_time after bound_begin
  job_cross_begin_remove=vector()
  job_cross_begin = data_cross_begin[,1]
  
  if(length(job_cross_begin)>0)
  {
    for(i0 in 1:length(job_cross_begin)){
      for(i1 in 1:length(data_w0_successful$job_id)){
        if(job_cross_begin[i0] == data_w0_successful$job_id[i1]){
          # jobs cross the begin bound is in data_w0_successful, we should recompute its usefultime and so on
          cross_start = data_w0_successful$starting_time[i1]
          cross_finish = data_w0_successful$finish_time[i1]
          cross_jobsize = data_w0_successful$requested_number_of_resources[i1]
          useful = (cross_finish-begin)*cross_jobsize
          data_w0_successful$useful[i1] = useful
          job_cross_begin_remove <- append(job_cross_begin_remove,i0)
          break
        }
      }
    }
    job_cross_begin = job_cross_begin[-job_cross_begin_remove] 
    job_cross_begin_remove = vector()
  }
  
  # deal with jobs cross the end bound
  data_cross_end <- data %>% filter(data[,9] < end & data[,11] > end) #starting_time before bound_end and finish_time after bound_end
  job_cross_end_remove=vector()
  job_cross_end = data_cross_end[,1]
  
  if(length(job_cross_end)>0)
  {
    for(i0 in 1:length(job_cross_end)){
      for(i4 in 1:length(data_w0_successful$job_id)){
        if(job_cross_end[i0] == data_w0_successful$job_id[i4]){
          # jobs cross the end bound is in data_w0_successful, we should recompute its usefultime and so on
          cross_start = data_w0_successful$starting_time[i4]
          cross_finish = data_w0_successful$finish_time[i4]
          cross_jobsize = data_w0_successful$requested_number_of_resources[i4]
          useful = (end - cross_start)*cross_jobsize
          data_w0_successful$useful[i4] = useful
          job_cross_end_remove <- append(job_cross_end_remove,i0)
          break
        }
      }
    }
    job_cross_end = job_cross_end[-job_cross_end_remove] 
    job_cross_end_remove = vector()
  }
  utilisation_ratio = sum(data_w0_successful$useful)/(nb_computing_machines*(end-begin))
  assign(paste("utilisation_",date_choice[ii],sep=""), utilisation_ratio)
}

date_index = c("June2017","March2018")
ratio = data.frame(
  heuristic = date_index,
  failure_free_utilisation = c(utilisation_2017June,utilisation_2018March)*100
)
print(xtable(ratio,digits=4, row.names=F), include.rownames = F)

setwd(paste(root_dir,"/Rtable",sep=""))
write.table (ratio, file ="Table3_utilisation.csv", row.names = FALSE)
