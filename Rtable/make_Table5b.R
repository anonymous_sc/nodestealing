rm(list=ls())
library(dplyr)
library(xtable)
setwd("../")
root_dir = getwd()

downtime = "downtime1h"
event_repeated_times=5
begin = 10*24*3600
end = 30*24*3600
date_choice = c("2017June","2018March")
for(ii in 1:length(date_choice)){
  date = date_choice[ii]
  wdfile = paste(root_dir,"/MIRA_simulation_data/MIRA", date,"/test_MIRA_", date,"_MTBF1h_", downtime, collapse = ", ", sep = "")
  setwd(wdfile)
  
  node_stealing_times_inbound_failure_times_repeat=vector()
  node_stealing_times_inbound_nodesteal_times_repeat=vector()
  for(mm in 0:(event_repeated_times-1)){
    node_stealing_times_file = paste("./node_stealing_times_", mm, "_111.txt", collapse = ", ", sep = "")
    node_stealing_times<-read.table(file = node_stealing_times_file,header=F,na.strings = c("NA"))
    
    node_stealing_times_beforebegin <- node_stealing_times %>% filter(node_stealing_times[,2]<begin)
    node_stealing_times_beforebegin_failure <-node_stealing_times_beforebegin%>% filter(node_stealing_times_beforebegin[,3]=="failure")
    node_stealing_times_beforebegin_failure_times=0
    if(length(node_stealing_times_beforebegin_failure[,1])>0){
      node_stealing_times_beforebegin_failure_times = max(node_stealing_times_beforebegin_failure$V4)
    }
    node_stealing_times_beforebegin_nodesteal<-node_stealing_times_beforebegin%>% filter(node_stealing_times_beforebegin[,3]=="nodesteal")
    node_stealing_times_beforebegin_nodesteal_times=0
    if(length(node_stealing_times_beforebegin_nodesteal[,1])>0){
      node_stealing_times_beforebegin_nodesteal_times = max(node_stealing_times_beforebegin_nodesteal$V4)
    }
    
    node_stealing_times_beforeend <- node_stealing_times %>% filter(node_stealing_times[,2]<end)
    node_stealing_times_beforeend_failure <-node_stealing_times_beforeend%>% filter(node_stealing_times_beforeend[,3]=="failure")
    
    node_stealing_times_beforeend_failure_times=0
    if(length(node_stealing_times_beforeend_failure[,1])>0){
      node_stealing_times_beforeend_failure_times = max(node_stealing_times_beforeend_failure$V4)
    }
    
    node_stealing_times_beforeend_nodesteal<-node_stealing_times_beforeend%>% filter(node_stealing_times_beforeend[,3]=="nodesteal")
    node_stealing_times_beforeend_nodesteal_times=0
    if(length(node_stealing_times_beforeend_nodesteal[,1])>0){
      node_stealing_times_beforeend_nodesteal_times = max(node_stealing_times_beforeend_nodesteal$V4)
    }
    
    node_stealing_times_inbound_failure_times = node_stealing_times_beforeend_failure_times - node_stealing_times_beforebegin_failure_times
    node_stealing_times_inbound_nodesteal_times = node_stealing_times_beforeend_nodesteal_times - node_stealing_times_beforebegin_nodesteal_times
    
    node_stealing_times_inbound_failure_times_repeat=append(node_stealing_times_inbound_failure_times_repeat,node_stealing_times_inbound_failure_times)
    node_stealing_times_inbound_nodesteal_times_repeat =append(node_stealing_times_inbound_nodesteal_times_repeat,node_stealing_times_inbound_nodesteal_times)
  }
  assign(paste("stealtimes_",date_choice[ii],sep=""), mean(node_stealing_times_inbound_nodesteal_times_repeat))
  assign(paste("emptytimes_",date_choice[ii],sep=""), mean(node_stealing_times_inbound_failure_times_repeat)-mean(node_stealing_times_inbound_nodesteal_times_repeat))
  assign(paste("failuretimes_",date_choice[ii],sep=""), mean(node_stealing_times_inbound_failure_times_repeat))
  
}

date_index = c("June2017","March2018")
ratio = data.frame(
  heuristic = date_index,
  nodestealing = c(stealtimes_2017June,stealtimes_2018March),
  emptyprocessor = c(emptytimes_2017June,emptytimes_2018March),
  totalfailures = c(failuretimes_2017June,failuretimes_2018March)
)
print(xtable(ratio,digits=4, row.names=F), include.rownames = F)

setwd(paste(root_dir,"/Rtable",sep=""))
write.table (ratio, file ="Table5b.csv", row.names = FALSE)
