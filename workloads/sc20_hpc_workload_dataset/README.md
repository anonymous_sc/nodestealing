This repository contains the job trace of jobs runs on Intrepid and Mira supercomputers from 2009 to 2018. The following job parameters are included in the trace:

UserID
ProjectID
QueueName
NumNodesRequested
NumCoresRequested
WallTimeRequested
QueuedTimestamp
StartTimestamp
EndTimestamp
EligibleQueueTime
Runtime
NodeSecondsUsed
CoreSecondsUsed