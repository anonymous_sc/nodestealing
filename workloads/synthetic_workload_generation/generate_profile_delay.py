import random
import numpy
import math
checkpoint_cost = 300 #checkpoint cost for the synthetic workload
total_processors = 128 #how many processors the platform has
total_MTBF = 128*1800 #MTBF for the whole platform is 30 minutes, for single node is 64 hours
simulation_with_failures=True #True means delay contains checkpoint cost(with failures), False means delay does not contain checkpoint cost(without failures)


total_failure_rate=1/total_MTBF #failure rate for single node
total_failure_rate_to_be_writen=total_failure_rate*10**8 #since the total_failure_rate is too small to recognize in c++, thus we times 10**8 here and divide 10**8 in c++
for j in range(0,7):
    res=2**j
    C=300
    #lambda_total=5*10**(-6)
    #T=((2*C)/(res*lambda_total))**(0.5)
    T=0
    temp=['A','B','C','D','E','F','G','H','I','J']
    #temp = range(0,64)
    numpy.random.seed(j)

    if simulation_with_failures:
        usefultime = numpy.random.uniform(low=60, high=7200, size=100)  # UNIFORM(1minute,2hours)
        ### For the failure simulation, let the runtime = usefultime+checkpoint_cost
        checkpoint_period = ((2 * checkpoint_cost) / (res * total_failure_rate)) ** (1 / 2)
        checkpoint_cost_total = numpy.floor(usefultime / checkpoint_period) * checkpoint_cost
        runtime = usefultime + checkpoint_cost_total

        for i in range(0, 100):
            profile = '\"delay.%s' % (temp[j]) + '.%d' % i + '\": {\"type\": \"delay\", \"delay\": %d,' % (
            runtime[i]) + ' \"np\": %d,' % (res) + \
                      ' \"checkpoint_period\": 0,\"checkpoint_cost\": 300, \"recovery_cost\": 300, \"Tfirst\": 0, \"Rfirst\": 0, \"total_failure_rate\":' + '%f' % total_failure_rate_to_be_writen + '},'
            x = str(profile) + '\n'
            with open("profiles_synthetic_more1jobs.txt", "a") as f:
                f.write(x)
    else:
        usefultime = numpy.random.uniform(low=60, high=7200, size=100)  # UNIFORM(1minute,2hours)
        ### For the failure-free simulation, withouC.json, just let the runtime = usefultime
        runtime = usefultime

        for i in range(0, 100):
            profile = '\"delay.%s' % (temp[j]) + '.%d' % i + '\": {\"type\": \"delay\", \"delay\": %d,' % (
                runtime[i]) + ' \"np\": %d,' % (res) + \
                      ' \"checkpoint_period\": 0,\"checkpoint_cost\": 300, \"recovery_cost\": 300, \"Tfirst\": 0, \"Rfirst\": 0, \"total_failure_rate\":' + '%f' % total_failure_rate_to_be_writen + '},'
            x = str(profile) + '\n'
            with open("profiles_synthetic_withoutC_more1jobs.txt", "a") as f:
                f.write(x)

