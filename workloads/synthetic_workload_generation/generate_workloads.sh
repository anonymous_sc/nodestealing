#!/usr/bin/bash

translate_script='translate_submission_times.py'

output_dir='generated_workloads'

mkdir -p ${output_dir}

for platform_size in 64 #the resources of job are: 1 2 4 8 16 32 64 
do
    for random_seed in 0
    do
        output_filename="synthetic_workload_seed${random_seed}_more1jobs.json"
        job_number=1000
        job_sizes_lognormal_mean=2.5     
        job_sizes_lognormal_standard_deviation=5
        maximum_job_length=100000
        
        python3 json_workload_generator.py -i 4 profiles/profiles_synthetic_more1jobs.json \
            "${output_dir}/${output_filename}" "${platform_size}" -jn "${job_number}" \
            -mu "${job_sizes_lognormal_mean}" -sigma "${job_sizes_lognormal_standard_deviation}" \
            -rs "${random_seed}" --maximum_job_length "${maximum_job_length}"

        python3 "${translate_script}" -i 4 -w "${output_dir}/${output_filename}"
    done
done
