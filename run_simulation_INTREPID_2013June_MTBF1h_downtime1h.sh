#!/usr/bin/bash

# run simulation of heuristic h0 and h111 for June 2013

printf "The INTREPID_2013June_MTBF1h_downtime1h simulation is running\n"
for e in {0..4}
do
    printf "The failure scenario %d is running\n" $e

    cd test_INTREPID_2013June_MTBF1h_downtime1h_${e}_0
    robin ./test_INTREPID_2013June_MTBF1h_downtime1h_${e}_0.yaml > outputfile 2> error_file &
    cd ..

    cd test_INTREPID_2013June_MTBF1h_downtime1h_${e}_111
    robin ./test_INTREPID_2013June_MTBF1h_downtime1h_${e}_111.yaml > outputfile 2> error_file &
    cd ..
done 

wait

printf "The INTREPID_2013June_MTBF1h_downtime1h without failures simulation is running\n"
# no node stealing, simple chechpoints using original conservative backfilling heuristic 2013June
cd test_INTREPID_2013June_MTBF1h_downtime1h_withoutC

robin ./test_INTREPID_2013June_MTBF1h_downtime1h_withoutC.yaml

cd ..