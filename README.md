Overview
========

This describes the source code, data and scripts used in our paper
"Improving Batch Schedulers with Node Stealing for Failed Jobs" 
whose revised version is submitted to SC. It also includes the
extended version of this revision:
- File `revisedSC22.pdf` is the submitted revised version of our paper.
- File `extended-revisedSC22.pdf` is the extended version of the above.

# Getting Started

The code was primarily developed on a debian-like system (Ubuntu 20.04 LTS).
The following version numbers are provided as indication of the versions that
were tested. We recommend to install the default and recent versions of Python and R.

## Install Python and R

R scripts were used with the following packages on R 4.1.1:

- dplyr 1.0.8
- ggplot2 3.3.5
- xtable 1.8-4
- assertthat 0.2.1

Recent versions of Python and R can be installed as follows on a
debian-like system (in a shell):

```shell
$ apt install python3 python3-pip r-base
```

Install all required R packages as follows within R (launched `R` in a shell):

```shell
$ R
> install.packages(c("dplyr", "ggplot2", "xtable", "assertthat"))
```

## Install Batsim

The best way to easily install all required packages for the
simulation is through the Nix package management system.

First, install Nix:

```shell
$ curl -L https://nixos.org/nix/install | sh
```

Second,  install Batsim in your system via Nix (original version). Note that this step may take  about 15 minutes.

```shell
$ nix-env -f https://github.com/oar-team/nur-kapack/archive/master.tar.gz -iA batsim
```

Third, install our version of Batsched:

```shell
$ git clone https://gitlab.com/anonymous_sc/nodestealing.git
Or
$ git clone git@gitlab.com:anonymous_sc/nodestealing.git
```

# Configurations

## Generate workloads

### For MIRA

For convenience, we include the MIRA traces with our code. However,
they may also be downloaded from [Mira traces](https://zenodo.org/record/3959499).

We now present how to generate our workload files from the MIRA traces.
In our simulations, we use two months from the MIRA traces: June 2017 and MIRA March 2018.

Generating the workload files is done with the
`workloads/generate_MIRA_workload.R` script. In the beginning of the
script, four parameters may be changed, to define the chosen year, the
chosen month, the MTBF of the whole platform, and a switch to tell if we
want to simulate failures. In case this switch is activated, we change
the duration (and walltime) of all jobs to account for the checkpoint times.

To run the script:
```shell
$ cd nodestealing/workloads
$ R -f generate_MIRA_workload.R 
```

This script also outputs the number of jobs categorized by size (Table
2 in our manuscript) and job length statistics (Table 3 in our
manuscript).  It takes a few seconds to generate MIRA workload files.
The workload files are put into `nodestealing/workloads` folder. Note
that for convenience, we have already provided the workload files used
in our simulation in this folder.

### For INTREPID

We now present how to generate our workload files from the INTREPID traces.

In our simulations, we use one month from the INTREPID traces: June 2013.

It's similar with the MIRA workload generation.

Generating the workload files is done with the
`workloads/generate_INTREPID_workload.R`script. 

To run the script:

```shell
$ cd nodestealing/workloads
$ R -f generate_INTREPID_workload.R 
```

It takes a few seconds to generate INTREPID workload files.

### For Synthetic

We now present how to generate our synthetic workload files.

Generating the workload files is done with the
`workloads/synthetic_workload_generation/generate_workloads.sh` script. 

To run the script:

```shell
$ cd nodestealing/workloads/synthetic_workload_generation
$ bash ./generate_workloads.sh #simulate with failures
$ bash ./generate_workloads_withoutC.sh #simulate without failures
```

It takes a few seconds to generate synthetic workload files.


## Generate failure events

### For MIRA

The next step is to generate the files describing the failure events
and unavailability times of the processors. The script for this task
is `events/events.py`. In the beginning of this script, one may change
the value of the downtime between processor failure and rejunevation,
the MTBF, the number of processors, the number of scenarios to
generate. For technical reasons, an upper bound on the length of the
simulation should be provided.

To run the script:

```shell
$ cd nodestealing/events
$ python3 events.py
```

It takes around 1 minute to generate failure event files.  The failure
event files should be put into `nodestealing/events` folder. Note that
we have already provided the failure event files used in our
simulation in this folder.

### For INTREPID

We now present how to generate the files describing the failure events for INTREPID.

It's similar with the MIRA failure files generation.

To run the script:

```shell
$ cd nodestealing/events
$ python3 events_intrepid.py
```

It takes around 1 minute to generate failure event files.

### For Synthetic

We now present how to generate the files describing the failure events for synthetic simulation.

It's similar with the MIRA failure files generation.

To run the script:

```shell
$ cd nodestealing/events
$ python3 events_synthetic.py
```

It takes around 1 minute to generate failure event files.

## Platform description

### For MIRA

In our simulation, we consider the MIRA platform which consists 49152
nodes, which is described in the file
`nodestealing/platforms/cluster49152.xml`. You may modify the number
of processors simply by changing the range 0-49151 on line 6 (in this
case, the number of processors should be changed accordingly in the
event generation script).

### For INTREPID

In our simulation, we consider the INTREPID platform which consists 40960
nodes, which is described in the file
`nodestealing/platforms/cluster40960.xml`. You may modify the number
of processors simply by changing the range 0-40959 on line 6 (in this
case, the number of processors should be changed accordingly in the
event generation script).

### For Synthetic

In our simulation, we consider the synthetic platform which consists 128
nodes, which is described in the file
`nodestealing/platforms/cluster128.xml`. You may modify the number
of processors simply by changing the range 0-127 on line 6 (in this
case, the number of processors should be changed accordingly in the
event generation script).

# Run simulations

Our node-stealing scheduler is implemented in batsched in a scheduler
`nodestealing/batsched/src/algo/conservative_bf_node_stealing.cpp`. The
baseline scheduler is
`nodestealing/batsched/src/algo/conservative_bf.cpp`. Note that
running the simulations takes much time, hence we already provide the
simulation results used in the paper.

## Running a small example for MIRA

Our version of batsched is built when you type `nix-shell`, so this step can take a few minutes. 

```shell
$ cd nodestealing
$ nix-shell
```
Then, the simulations may be run in the nix-shell.

Here, we give an example on how to do the simulation for the Baseline and SFSJ heuristic.
The workload is MIRA of June 2017.  MTBF is 1h and downtime is 1h with five failure scenarios. The platform contains 49152 processors.

### Generate simulation configuration files
In  the nodestealing directory, type:
```shell
$ bash ./generate_yaml_MIRA_2017June_MTBF1h_downtime1h.sh
```

### Run simulations
```shell
$ bash ./run_simulation_MIRA_2017June_MTBF1h_downtime1h.sh
```

### Catalog the simulation results
```shell
$ bash ./catalog_results_MIRA_2017June_MTBF1h_downtime1h.sh
```

Warning: note that it takes around 1 hour to run all simulations for
this setting.

Then, you get the folder `test_MIRA_2017June_MTBF1h_downtime1h` containing the simulation data.


## Running all simulations for MIRA

Here, we present how to do the simulation for the Baseline and all node-stealing heuristic variants .
Note that it will take around 3 days to rerun the simulation.
Thus, we put all the simulation result data in the folder `MIRA_simulation_data`, you can use these data to reproduce all the tables and figures in our manuscript directly by skipping this part.

The following commands produce the results for two months of the MIRA trace: June 2017 and March 2018, with five failure scenarios for the six MTBF variants:  20min, 40min, 1h, 2h, 5h and 10h, for the three downtime variants: 10min, 1h and 1day, and the eighteen different node-stealing heuristic variants: Varaint111, Varaint112, ... , Varaint323. 


### Generate simulation configuration files
```shell
$ bash ./generate_yaml_MIRA.sh
```

### Run the simulation
```shell
$ bash ./run_simulation_MIRA.sh
```

### Catalog the simulation results
```shell
$ bash ./catalog_results_MIRA.sh
```

## Running simulations for INTREPID

Here, we give an example on how to do the simulation for the Baseline and SFSJ heuristic.
The workload is INTREPID of June 2013.  MTBF is 1 hour and downtime is 1 hour with five failure scenarios. The platform contains 40960 processors.

### Generate simulation configuration files

In  the nodestealing directory, type:

```shell
$ bash ./generate_yaml_INTREPID_2013June_MTBF1h_downtime1h.sh 
```

### Run simulations

```shell
$ bash ./run_simulation_INTREPID_2013June_MTBF1h_downtime1h.sh
```

### Catalog the simulation results

```shell
$ bash ./catalog_results_INTREPID_2013June_MTBF1h_downtime1h.sh
```

Warning: note that it takes around 1 hour to run all simulations for
this setting.

Then, you get the folder `test_INTREPID_2013June_MTBF1h_downtime1h` containing the simulation data.

## Running simulations for Synthetic workload

Here, we give an example on how to do the simulation for the Baseline and SFSJ heuristic.
The workload is generated synthetically.  MTBF is 30 minutes and downtime is 10 minutes with five failure scenarios. The platform contains 128 processors.

### Generate simulation configuration files

In  the nodestealing directory, type:

```shell
$ bash ./generate_yaml_synthetic.sh 
```

### Run simulations

```shell
$ bash ./run_simulation_synthetic.sh
```

### Catalog the simulation results

```shell
$ bash ./catalog_results_synthetic.sh
```

It takes around 5 minutes to run all simulations for this setting.

Then, you get the folder `test_synthetic` containing the simulation data.

# Analyze the data and reproduce figures

We use R to analyze the data, make tables and plot figures.

## Reproduce figures

You can use these commands to reproduce figures in our manuscript directly.

```shell
$ cd nodestealing/Rplot
```

```shell
$ R -f plot_Figure1.R # Figure 1 for MIRA 
$ R -f plot_Figure1_Intrepid.R # Figure 1 for Intrepid 
$ R -f plot_Figure1_synthetic.R # Figure 1 for synthetic workload
$ R -f plot_Figure3.R # Figure 3
$ R -f plot_Figure4.R # Figure 4 
$ R -f plot_Figure5.R # Figure 5 for MIRA
$ R -f plot_Figure5_Intrepid.R # Figure 5 for Intrepid
$ R -f plot_Figure5_synthetic.R # Figure 5 for synthetic workload
$ R -f plot_Figure6.R # Figure 6
$ R -f plot_Figure7a.R # Figure 7a
$ R -f plot_Figure7b.R # Figure 7b
$ R -f plot_Figure8.R # Figure 8
$ R -f plot_Figure9a.R # Figure 9a
$ R -f plot_Figure9b.R # Figure 9b
$ R -f plot_Figure15.R # Figure 15
$ R -f plot_Figure16.R # Figure 16
$ R -f plot_Figure17.R # Figure 17
```

## Reproduce tables

You can use these commands to reproduce tables in our manuscript directly.

```shell
$ cd nodestealing/Rtable
```

```shell
$ R -f make_Table3_utilisation.R # Table3_utilisation for MIRA
$ R -f make_Table3_utilisation_intrepid # Table3_utilisation for INTREPID
$ R -f make_Table4.R # Table4 for MIRA
$ R -f make_Table4_Intrepid # Table4 for INTREPID
$ R -f make_Table4_synthetic # Table4 for synthetic workload
$ R -f make_Table5a.R # Table5a for MIRA
$ R -f make_Table5a_Intrepid # Table5a for INTREPID
$ R -f make_Table5b.R # Table5b for MIRA
$ R -f make_Table5b_Intrepid # Table5b for INTREPID
$ R -f make_Table6_utilisation.R # Table6_utilisation
$ R -f make_Table6_idleperc.R # Table6_idleperc
$ R -f make_Table7.R # Table7
```
