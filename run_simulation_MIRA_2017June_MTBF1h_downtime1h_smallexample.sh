#!/usr/bin/bash

# run simulation of heuristic h0 and h111 for June 2017

printf "The MIRA_2017June_MTBF1h_downtime1h simulation is running\n"
for e in 0
do
    printf "The failure scenario %d is running\n" $e

    cd test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample
    robin ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample.yaml
    cd ..

    cd test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample
    robin ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample.yaml
    cd ..
done 
