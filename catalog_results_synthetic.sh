#!/usr/bin/bash

### catalog h0 and h111 simulation results for synthetic

mkdir test_synthetic

for e in {0..4}
do
    cp ./test_synthetic_${e}_0/COMPLETED_KILLED.txt ./test_synthetic/COMPLETED_KILLED_${e}_0.txt
    cp ./test_synthetic_${e}_0/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_synthetic/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_0.txt
    cp ./test_synthetic_${e}_0/node_stealing_info.txt ./test_synthetic/node_stealing_info_${e}_0.txt
    cp ./test_synthetic_${e}_0/log_${e}_0/expe_${e}_0_jobs.csv ./test_synthetic/expe_${e}_0_jobs.csv
    cp ./test_synthetic_${e}_0/log_${e}_0/expe_${e}_0_machine_states.csv ./test_synthetic/expe_${e}_0_machine_states.csv
    cp ./test_synthetic_${e}_0/log_${e}_0/expe_${e}_0_schedule.csv ./test_synthetic/expe_${e}_0_schedule.csv


    cp ./test_synthetic_${e}_111/COMPLETED_KILLED.txt ./test_synthetic/COMPLETED_KILLED_${e}_111.txt
    cp ./test_synthetic_${e}_111/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_synthetic/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_111.txt
    cp ./test_synthetic_${e}_111/node_stealing_info.txt ./test_synthetic/node_stealing_info_${e}_111.txt
    cp ./test_synthetic_${e}_111/node_stealing_times.txt ./test_synthetic/node_stealing_times_${e}_111.txt
    cp ./test_synthetic_${e}_111/log_${e}_111/expe_${e}_111_jobs.csv ./test_synthetic/expe_${e}_111_jobs.csv
    cp ./test_synthetic_${e}_111/log_${e}_111/expe_${e}_111_machine_states.csv ./test_synthetic/expe_${e}_111_machine_states.csv
    cp ./test_synthetic_${e}_111/log_${e}_111/expe_${e}_111_schedule.csv ./test_synthetic/expe_${e}_111_schedule.csv
done 

for e in {0..4}
do
    rm -r test_synthetic_${e}_0
    rm -r test_synthetic_${e}_111
done 