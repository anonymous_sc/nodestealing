#!/usr/bin/bash

### catalog h0 and h111 simulation results for June 2017

mkdir -p test_MIRA_2017June_MTBF1h_downtime1h_smallexample

for e in 0
do
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample/COMPLETED_KILLED.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/COMPLETED_KILLED_${e}_0.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_0.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample/node_stealing_info.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/node_stealing_info_${e}_0.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample/log_${e}_0/expe_${e}_0_jobs.csv ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/expe_${e}_0_jobs.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample/log_${e}_0/expe_${e}_0_machine_states.csv ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/expe_${e}_0_machine_states.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample/log_${e}_0/expe_${e}_0_schedule.csv ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/expe_${e}_0_schedule.csv


    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/COMPLETED_KILLED.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/COMPLETED_KILLED_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/node_stealing_info.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/node_stealing_info_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/node_stealing_times.txt ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/node_stealing_times_${e}_111.txt
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/log_${e}_111/expe_${e}_111_jobs.csv ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/expe_${e}_111_jobs.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/log_${e}_111/expe_${e}_111_machine_states.csv ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/expe_${e}_111_machine_states.csv
    cp ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample/log_${e}_111/expe_${e}_111_schedule.csv ./test_MIRA_2017June_MTBF1h_downtime1h_smallexample/expe_${e}_111_schedule.csv
done 

for e in 0
do
    rm -r test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample
    rm -r test_MIRA_2017June_MTBF1h_downtime1h_${e}_111_smallexample
done 