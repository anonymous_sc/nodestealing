#!/usr/bin/bash

# run simulation of heuristic h0 and h111 for June 2017

printf "The MIRA_2017June_MTBF1h_downtime1h simulation is running\n"
for e in {0..4}
do
    printf "The failure scenario %d is running\n" $e

    cd test_MIRA_2017June_MTBF1h_downtime1h_${e}_0
    robin ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_0.yaml > outputfile 2> error_file 
    cd ..

    cd test_MIRA_2017June_MTBF1h_downtime1h_${e}_111
    robin ./test_MIRA_2017June_MTBF1h_downtime1h_${e}_111.yaml > outputfile 2> error_file
    cd ..
done 
