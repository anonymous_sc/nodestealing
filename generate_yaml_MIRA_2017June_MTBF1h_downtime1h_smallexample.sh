#!/usr/bin/bash

### generate yaml files of heuristic h111 for June 2017

for e in 0
do
    mkdir -p test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample
    cd test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample

    # no node stealing, simple chechpoints
    robin generate "test_MIRA_2017June_MTBF1h_downtime1h_${e}_0_smallexample.yaml" \
    --output-dir "log_${e}_0" \
    --batcmd "batsim -p ../platforms/cluster500.xml -w ../workloads/MIRA_2017June_MTBF1h_smallexample.json -e log_${e}_0/expe_${e}_0 --enable-dynamic-jobs --enable-profile-reuse --acknowledge-dynamic-jobs --forward-profiles-on-submission --events ../events/events_MIRA_MTBF1h_downtime1h_${e}_smallexample.txt --disable-schedule-tracing --socket-endpoint ipc://foobar_${e}_0" \
    --schedcmd "batsched -v conservative_bf_node_stealing --variant_options={\\\"heuristic_choice\\\":0\,\\\"victim_choice\\\":1\,\\\"decision_choice\\\":1} --queue_order fcfs_prio --socket-endpoint ipc://foobar_${e}_0" 

    cd ..
    
    # all 18 node stealing heuristics
    for h in 1
    do
        for v in 1
        do
            for k in 1
            do
                mkdir -p test_MIRA_2017June_MTBF1h_downtime1h_${e}_$h$v${k}_smallexample
                cd test_MIRA_2017June_MTBF1h_downtime1h_${e}_$h$v${k}_smallexample

                robin generate "test_MIRA_2017June_MTBF1h_downtime1h_${e}_$h$v${k}_smallexample.yaml" \
                --output-dir "log_${e}_$h$v$k" \
                --batcmd "batsim -p ../platforms/cluster500.xml -w ../workloads/MIRA_2017June_MTBF1h_smallexample.json -e log_${e}_$h$v$k/expe_${e}_$h$v$k --enable-dynamic-jobs --enable-profile-reuse --acknowledge-dynamic-jobs --forward-profiles-on-submission --events ../events/events_MIRA_MTBF1h_downtime1h_${e}_smallexample.txt --disable-schedule-tracing --socket-endpoint ipc://foobar_${e}_$h$v$k" \
                --schedcmd "batsched -v conservative_bf_node_stealing --variant_options={\\\"heuristic_choice\\\":$h\,\\\"victim_choice\\\":$v\,\\\"decision_choice\\\":$k} --queue_order fcfs_prio --socket-endpoint ipc://foobar_${e}_$h$v$k" 
            
                cd ..
            done
        done
    done 
done
